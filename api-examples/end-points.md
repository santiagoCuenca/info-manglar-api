## SOME END POINTS EXAMPLES


# GET get organizations
http://23.20.94.236/info-manglar/rest/organization-manglar/get

# GET get allowed users by type
http://23.20.94.236/info-manglar/rest/allowed-user/get/socio
http://23.20.94.236/info-manglar/rest/allowed-user/get/org
http://23.20.94.236/info-manglar/rest/allowed-user/get/mae
http://23.20.94.236/info-manglar/rest/allowed-user/get/inp
http://23.20.94.236/info-manglar/rest/allowed-user/get/super-admin


# POST save allowed user
http://23.20.94.236/info-manglar/rest/allowed-user/save/socio
{
	"allowedUser": {
		"allowedUserId": null,
		"allowedUserStatus": true,
		"allowedUserName": "socio22",
		"allowedUserPin": "1104382393"
	},
	"organizationManglarId": 1,
	"geloId": 1
}


# GET get ALL forms data
http://23.20.94.236/info-manglar/rest/<form-id>/get

# GET ger price form
http://23.20.94.236/info-manglar/rest/prices-form/get

# POST save price form
http://23.20.94.236/info-manglar/rest/prices-form/save

{
   "pricesFormId":14,
   "formType":"prices-form",
   "userId":107,
   "organizationManglarId":9,
   "priceDailies":[
      {
         "id":21,
         "productType":"Productos bioacuáticos",
         "bioAquaticType":"Pulpa de Cangrejo",
         "bioAquaticPrice":2.0,
         "crabPulpPounds":12.0,
         "fileForms":[
            {
               "id":92,
               "name":"o9productImage1579313151094.jpg",
               "type":"photo",
               "url":"http://10.0.2.2:8080/info-manglar/rest/file/images/o9productImage1579313151094.jpg",
               "idOption":"productImage",
               "alfrescoCode":"alfresco"
            }
         ]
      },
      {
         "id":22,
         "productType":"Productos bioacuáticos",
         "bioAquaticType":"Concha",
         "shellQuality":"Normal",
         "bioAquaticPrice":3.0,
         "shellCount":200,
         "fileForms":[

         ]
      },
      {
         "id":23,
         "productType":"Productos bioacuáticos",
         "bioAquaticType":"Concha",
         "shellQuality":"Normal",
         "bioAquaticPrice":4.0,
         "shellCount":200,
         "fileForms":[

         ]
      }
   ],
   "name":"mss21",
   "mobile":"090921",
   "address":"saas"
}
