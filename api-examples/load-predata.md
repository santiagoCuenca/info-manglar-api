## HOW TO RUN infosuia.sql on UBUNTU SERVER

# stop jboss service
sudo service jbossas6 stop

# POSTGRES
$ sudo -i -u postgres
$ psql

# ROLES
\c postgres
create role read;
create role postgres;
create role suia_iii_escritura;

# DATA BASE CLEAN
drop database suia;
create database suia;
\c suia
\i /<FILE_PATH>/infosuia.sql

# Exit
\q
